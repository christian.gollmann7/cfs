// -*- mode: c++; coding: utf-8; indent-tabs-mode: nil; -*-
// vim: set ts=2 sw=2 et nu ai ft=cpp cindent !:
// kate: space-indent on; indent-width 2; encoding utf-8;
// kate: auto-brackets on; mixedindent off; indent-mode cstyle;
// ================================================================================================
/*!
 *       \file     CoefFunctionPML.hh
 *       \brief    <Description>
 *
 *       \date     Mar 12, 2012
 *       \author   ahueppe
 */
//================================================================================================

#ifndef COEFFUNCTIONPML_HH_
#define COEFFUNCTIONPML_HH_

#define _USE_MATH_DEFINES
#include <cmath>

#include "CoefFunction.hh"

namespace CoupledField{

class DampFunction{

public:
  typedef enum{ NO_TYPE, CONSTANT, INVERSE_DIST, QUADRATIC, SMOOTH, TANGENS, RATIONAL, EXPONENTIAL, POLY_DIRECT, POLY_INVERSE } DampingType;
  static Enum<DampingType> DampingTypeEnum;

  DampFunction(){
    ReflectionCoefficient = 1e-3;
    functionType = NO_TYPE;
    DampFactor = 1.0;
    constFactor =1.0;
  }

  virtual ~DampFunction() {};

  virtual Double ComputeFactor(Double pos, Double thickness)=0;


  DampingType GetType(){return functionType;}

  Double DampFactor;

protected:
  Double constFactor;
  Double ReflectionCoefficient;
  DampingType functionType;


};

class DampFunctionConst : public DampFunction{
public:
  DampFunctionConst(Double SpeedOfSound) : DampFunction(){
    constFactor = -1.0 * SpeedOfSound * log(ReflectionCoefficient)/2.0;
    functionType = CONSTANT;
  }

  Double ComputeFactor(Double pos, Double thickness){
    return DampFactor * constFactor / thickness;
  }

};

class DampFunctionQuad : public DampFunction{
public:
  DampFunctionQuad(Double SpeedOfSound) : DampFunction(){
    constFactor = -3.0 * SpeedOfSound * log(ReflectionCoefficient)/2.0;
    functionType = QUADRATIC;
  }

  Double ComputeFactor(Double pos, Double thickness){
    return DampFactor*constFactor * pos * pos / (thickness * thickness*thickness);
  }

};

class DampFunctionInvDist : public DampFunction{
public:
  DampFunctionInvDist(Double SpeedOfSound) : DampFunction(){
    constFactor = SpeedOfSound;
    functionType = INVERSE_DIST;
  }

  Double ComputeFactor(Double pos, Double thickness){
    //check for sigular position
    Double val = 0.0;
    if(thickness == pos){
      //ok we just take a high value....
      val = constFactor * (1.0 / 1e-10);
    }else{
      val = constFactor * (1.0 / (thickness - pos));
    }
    return val*DampFactor;
  }

};

class DampFunctionSmooth : public DampFunction{
public:
  DampFunctionSmooth(Double SpeedOfSound) : DampFunction(){
    constFactor = SpeedOfSound * log(1.0/ReflectionCoefficient);
    functionType = SMOOTH;
  }

  Double ComputeFactor(Double pos, Double thickness){
    Double value = constFactor/thickness;
    value *= ( (pos / thickness) - ((sin(2*M_PI*pos / thickness)/(8*atan(1.0))) ) );
    return value*DampFactor;
  }

};

class DampFunctionTangens : public DampFunction{
public:
  DampFunctionTangens( ) : DampFunction(){
    constFactor = 2.0/M_PI;
    functionType = TANGENS;
  }
  // The factor is the derivative of the mapping function
  Double ComputeFactor(Double z, Double sos){
    //Double z = pos/thickness;
    //Double x = DampFactor*tan(z*M_PI/2.0);
    //return 2.0*DampFactor/(M_PI*(DampFactor*DampFactor+x*x));
    Double L = DampFactor*sos; // L corresponds to kappa in the IML paper
    Double c = cos(z/constFactor);
    return c*c/L; // same but possibly faster
  }

};

class DampFunctionRational : public DampFunction{
public:
  DampFunctionRational( ) : DampFunction(){
    constFactor = 1.0;
    functionType = RATIONAL;
  }

  Double ComputeFactor(Double z, Double sos){
    //Double z = pos/thickness; // coordinate in layer
    Double L = DampFactor*sos; // L corresponds to kappa in the IML paper
    Double x = z*L/(1.0 - z); // x-coordinate
    return L/((x+L)*(x+L)); // dz/dx=eta(x)=eta(x(z))
  }

};

class DampFunctionExponential : public DampFunction{
public:
  DampFunctionExponential( ) : DampFunction(){
    constFactor = 1.0;
    functionType = EXPONENTIAL;
  }

  Double ComputeFactor(Double z, Double sos){
    //Double z = pos/thickness; // local coordinate in layer [0,1]
    Double L = DampFactor*sos;
    return (1-z)/L;
  }

};

class DampFunctionPolyDirect : public DampFunction
{
private:
  UInt power_;

public:
  DampFunctionPolyDirect(UInt power) : DampFunction()
  {
    power_ = power;
    constFactor = -0.5*(power_ + 1)*log(ReflectionCoefficient);
    functionType = POLY_DIRECT;
  }

  Double ComputeFactor(Double pos, Double thickness)
  {
    Double value = pow(pos/thickness, power_);
    return DampFactor*value;
  }
};

class DampFunctionPolyInverse : public DampFunction
{
private:
  UInt power_;

public:
  DampFunctionPolyInverse(UInt power) : DampFunction()
  {
    power_ = power;
    constFactor = -0.5*(power_ + 1)*log(ReflectionCoefficient);
    functionType = POLY_INVERSE;
  }

  Double ComputeFactor(Double pos, Double thickness)
  {
    Double value = pow(1.0 - pos/thickness, power_);
    return DampFactor*value;
  }
};

template<typename T>
class CoefFunctionPML : public CoefFunction{

public:

  //! Enumeration data type describing formulations of PML
  typedef enum{ CLASSIC, SHIFTED } PMLFormulType;

  CoefFunctionPML(PtrParamNode pmlDef, PtrCoefFct speedOfSound,
                  shared_ptr<EntityList> EntList,
                  StdVector<RegionIdType> pdeDomains,
                  bool isVector );

  virtual ~CoefFunctionPML();

  virtual string GetName() const { return "CoefFunctionPML"; }


  //! Return real-valued tensor at integration point
  virtual void GetTensor(Matrix<Complex>& tensor,
                 const LocPointMapped& lpm );

  //! Return real-valued tensor at integration point
  virtual void GetTensor(Matrix<Double>& tensor,
                 const LocPointMapped& lpm );

  //! Return real-valued vector at integration point
  virtual void GetVector(Vector<Complex>& vec,
                 const LocPointMapped& lpm ) ;

  //! Return real-valued vector at integration point
  virtual void GetVector(Vector<Double>& vec,
                 const LocPointMapped& lpm ) ;


 //! Return real-valued scalar at integration point
  // this is little bit of a hack,
  // seeing that the jacobian is transformed according to the changed
  // derivatives, we pass this function as a scalar function to the bilinearform
  // an transform the jacobian with it....
  virtual void GetScalar(Double& val,
                const LocPointMapped& lpm ) ;

 //! Return cpmplex-valued scalar at integration point
  virtual void GetScalar(Complex& val,
                const LocPointMapped& lpm ) ;

 //! \copydoc CoefFunction::GetVecSize
 UInt GetVecSize() const {
   assert(this->dimType_ == VECTOR );
   return dim_;
 }
 

  void AddEntityList(shared_ptr<EntityList>){
    EXCEPTION("Add Entities may not be called in PML CoefFunction. Specify the region in the constructor!");
  }

  virtual bool IsComplex(){
    return std::is_same<T,Complex>::value;
  }

  std::string ToString() const {
      std::string out = "CoefFunctionPML";
      return out;
  };

protected:
    //void ComputeDampingFactor( Vector<Double>& factors,
    //                           const LocPointMapped& lpm,
    //                           UInt dir);
    //
    //void ComputeDampingFactor(Vector<Complex>& factors,
    //                          const LocPointMapped& lpm,
    //                          UInt dir);
    //
    //! Call-back method for re-calculation
    void UpdateOmega();

    void SetPosPML(Matrix<Double> & inner,
                   Matrix<Double> & outer);

    void ReadDataPML(PtrParamNode pmlDef,StdVector<RegionIdType> pdeDomains);

    void CreateDampFunction();

    void GuessLayerData(StdVector<RegionIdType> pdeDomains);

    void GetThicknessAtPoint(Double& thickness,Double& position, LocPointMapped lpm,UInt dir);

    PMLFormulType formulationType_;

    Matrix<Double> innerMinMaxComp_;
    Matrix<Double> outerMinMaxComp_;

    //! Support of the CoefFunction. Only needed for grid/solution results
    StdVector<shared_ptr<EntityList> > entities_;
    
    shared_ptr<DampFunction> dampFunction_;

    DampFunction::DampingType pmlType_;

    //! should be coefFunction in future...
    PtrCoefFct speedOfSound_;

    //! Pointer to math parser instance
    MathParser* mp_;

    //! Handle for expression
    unsigned int mHandle_;

    //! storing the current frequency
    Double omega_;
    
    //! dimension of the problem
    UInt dim_;

    //! flag, if PML coefficient functions describes the vector 
    bool isVector_;

};

template<typename T>
class CoefFunctionShiftedPML : public CoefFunctionPML<T>
{

public:

  CoefFunctionShiftedPML(PtrParamNode pmlDef, PtrCoefFct speedOfSound, shared_ptr<EntityList> EntList,
                         StdVector<RegionIdType> pdeDomains, bool isVector);

  virtual ~CoefFunctionShiftedPML();

  virtual string GetName() const { return "CoefFunctionShiftedPML"; }

  //! \copydoc CoeffFunctionPML::GetTensor
  virtual void GetTensor(Matrix<Complex>& tensor, const LocPointMapped& lpm);

  //! \copydoc CoeffFunctionPML::GetVector
  virtual void GetVector(Vector<Complex>& vector, const LocPointMapped& lpm);

  //! \copydoc CoeffFunctionPML::GetScalar
  virtual void GetScalar(Complex& scalar, const LocPointMapped& lpm);

  using CoefFunctionPML<T>::GetTensor;

  using CoefFunctionPML<T>::GetVector;

  using CoefFunctionPML<T>::GetScalar;

protected:

  PtrCoefFct scalingCoef_, shiftCoef_;

  shared_ptr<DampFunction> scalingFunc_, shiftFunc_;
};

}
#endif /* COEFFUNCTIONPML_HH_ */
