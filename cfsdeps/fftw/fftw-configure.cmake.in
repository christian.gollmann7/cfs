# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
SET(fftw_source "@fftw_source@")
SET(CFS_BINARY_DIR "@CFS_BINARY_DIR@")
SET(LIB_SUFFIX "@LIB_SUFFIX@")
SET(OPENMP_FOUND "@OPENMP_FOUND@")
SET(CMAKE_STATIC_LIBRARY_PREFIX "@CMAKE_STATIC_LIBRARY_PREFIX@")
SET(CMAKE_STATIC_LIBRARY_SUFFIX "@CMAKE_STATIC_LIBRARY_SUFFIX@")
SET(CMAKE_TOOLCHAIN_PREFIX "@CMAKE_TOOLCHAIN_PREFIX@")

#=============================================================================
# Set environment variables for configure process.
#=============================================================================
SET(ENV{CC} "@CMAKE_C_COMPILER@")
SET(ENV{CFLAGS} "@CFSDEPS_C_FLAGS@")

find_program(SH sh)

SET(CONFIGURE_CMD "${SH}")

LIST(APPEND CONFIGURE_CMD 
  "./configure"
  "--prefix=${CFS_BINARY_DIR}"
  "--bindir=${CFS_BINARY_DIR}/bin"
  "--libdir=${CFS_BINARY_DIR}/${LIB_SUFFIX}"
  "--enable-sse2"
  "--enable-threads")

LIST(APPEND CONFIGURE_CMD
  "--disable-shared"
  "--enable-static")


IF(OPENMP_FOUND)
  LIST(APPEND CONFIGURE_CMD "--enable-openmp")
ENDIF()

EXECUTE_PROCESS(
  COMMAND ${CONFIGURE_CMD}
  WORKING_DIRECTORY "${fftw_source}"
  ERROR_FILE fftw-config.out
  OUTPUT_FILE fftw-config.out
  RESULT_VARIABLE rv
  )
